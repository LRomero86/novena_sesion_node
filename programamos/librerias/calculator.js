function sumar(val1, val2){
    if(validar(val1) == true && validar(val2) == true){
        return val1 + val2;
    }else{
        return console.log('Error en los valores');
    }
}

function restar(val1, val2){
    if(validar(val1) == true && validar(val2) == true){
        return val1 - val2;
    }else{
        return console.log('Error en los valores');
    }
}

function multiplicar(val1, val2){
    if(validar(val1) == true && validar(val2) == true){
        return val1 * val2;
    }else{
        return console.log('Error en los valores');
    }
}

function dividir(val1, val2){
    if(validar(val1) == true && validar(val2) == true && val2 != 0){
        return val1 / val2;
    }else{
        return console.log('Error en los valores');
    }
}

function validar(val){
    if(isNaN(val)===false){
        console.log('es un número: ', val);
        return true;
    }else{
        return false;
    }
}

exports.sumar = sumar;
exports.restar = restar;
exports.multiplicar = multiplicar;
exports.dividir = dividir;