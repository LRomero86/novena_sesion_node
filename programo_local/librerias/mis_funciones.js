function sumar(val_1, val_2){
    return val_1 + val_2;
}

function restar(val_1, val_2){
    return val_1 - val_2;
}

function multiplicar(val_1, val_2){
    return val_1 * val_2;    
}

function dividir(val_1, val_2){
    return val_1 / val_2;
}

var msg_ok = "La operación se realizó correctamente";
var msg_fail = "La operación no se pudo completa";
exports.sumar = sumar;
exports.restar = restar;
exports.multiplicar = multiplicar;
exports.dividir = dividir;
exports.msg_ok = msg_ok;
exports.msg_fail = msg_fail;