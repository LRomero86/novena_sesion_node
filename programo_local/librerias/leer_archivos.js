const fs = require('fs');

function leer_archivo(file){
    
    return fs.readFileSync(file, 'utf8');

}

exports.leer_archivo = leer_archivo;