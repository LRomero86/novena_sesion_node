const fs = require('fs');

const folderPath = './';
//mostrar el listado de archivos en la carpeta

function mostrar_archivos(){
    fs.readdir(folderPath, (err,files) => {
        files.forEach(file => {
            console.log(file);
        });
    });
}

exports.mostrar_archivos = mostrar_archivos;