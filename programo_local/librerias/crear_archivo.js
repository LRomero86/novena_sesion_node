const fs = require('fs'); // 'fs' es File System

//función que permite creación de archivos
//params: file >el nombre del archivo por ejemplo log.txt
// params: text > el contenido del archivo, hola, saludos, etc.
function crear_archivo(file, text){
    fs.appendFile(file,text + "\n", function(err){
        if(err){
            return console.log(err);
        }
    });
}

exports.crear_archivo = crear_archivo;